class Comment < ActiveRecord::Base
	belongs_to :post
	validates_presence_of :post_id
	validates_numericality_of :post_id, :only_integer => true, :greater_than => 0
	validates_presence_of :body
end
